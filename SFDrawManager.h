#pragma once
#ifndef SFDRAWMANAGER_H
#define SFDRAWMANAGER_H
#include <list>
#include <vector>

#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "Player.h"

class Card;


class SFDrawManager
{
public:
	
	// UI texture and filename
	sf::Texture m_BackgroundTexture;
	std::string m_BackgroundTextureFileName = "board.jpg";
	sf::Texture m_StartIconTexture;
	std::string m_StartIconTextureFileName = "start.png";
	sf::Texture m_MeleeIconTexture;
	std::string m_MeleeIconTextureFileName = "Melee.png";
	sf::Texture m_RangedIconTexture;
	std::string m_RangedIconTextureFileName = "Ranged.png";
	sf::Texture m_SiegeIconTexture;
	std::string m_SiegeIconTextureFileName = "Siege.png";
	sf::Sprite m_BackgroundSprite;

	sf::Font m_font;
	std::string m_FontFileName = "MorrisRomanBlack.ttf";
	sf::SoundBuffer m_buffer;
	std::string m_SoundFileName = "main_theme.ogg";
	sf::Sound m_sound;

private:
	int m_ScreenWidth = 1600;
	int m_ScreenHeight = 1200;
	sf::RenderWindow* m_window = nullptr;
	sf::View m_view;
	std::list<sf::Text*> m_TextList;
	std::list<sf::Sprite*> m_SpriteList;

public:

	SFDrawManager();
	~SFDrawManager();

	void Initialize();

	void ClearScreen();
	
	void DisplayMain();

	int DisplayDeckPick();

	sf::Text* GenerateText(int _fontsize, sf::Color _color, std::string, std::string _positionString);
	sf::Sprite* GenerateSprite(sf::Texture* _texturePtr, std::string _textureFile, std::string _positionString);

	void DrawObjectOnView(float _positionX, float _positionY, sf::Text &_text);
	void DrawObjectOnView(float _positionX, float _positionY, sf::Sprite &_sprite);

	int UpdateGameUIOnPlayerMove(Card*& _cardLastClicked, int _textClicked, Player* _player, Player* _AI, int _roundNumber);
	void UpdateGameUI(Card*& _cardLastClicked, int _textClicked, Player* _player, Player* _AI, int _roundNumber);

	bool isPlayerClickedTarget(sf::Event _event, sf::Sprite* _targetSprite);
	bool isPlayerClickedTarget(sf::Event _event, sf::Text* _targetText);

	int GetPlayerClickedText(sf::Event _event, std::vector<sf::Text*> _targetTexts);

	Card* GetPlayerClickedCard(sf::Event _event, std::list<Card*> _targetCards);

	std::string BoolToString(bool _isTrue);

	void LoadGameOverScreen(std::string _message);
};

#endif // !SFDRAWMANAGER_H
