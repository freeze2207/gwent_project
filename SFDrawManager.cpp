#include <iostream>

#include "SFDrawManager.h"
#include "Card.h"

SFDrawManager::SFDrawManager()
{
}

SFDrawManager::~SFDrawManager()
{
}

void SFDrawManager::Initialize()
{
	m_window = new sf::RenderWindow(sf::VideoMode(m_ScreenWidth, m_ScreenHeight), "Gwent Duel");
	m_window->setFramerateLimit(60);
	m_view.reset(sf::FloatRect(0, 0, m_ScreenWidth, m_ScreenHeight));
	m_window->setView(m_view);

	// prepare the background
	if (m_BackgroundTexture.loadFromFile(m_BackgroundTextureFileName) == false)
	{
		std::cout << "Error loading the BackGround Texture" << std::endl;
		return;
	}
	m_BackgroundSprite.setTexture(m_BackgroundTexture);

	// prepare the font 
	if (m_font.loadFromFile(m_FontFileName) == false)
	{
		std::cout << "Error loading the Font" << std::endl;
		return;
	}

	// prepare Sound 

	if (m_buffer.loadFromFile(m_SoundFileName) == false)
	{
		std::cout << "Error loading the sound" << std::endl;
		return;
	}
	m_sound.setBuffer(m_buffer);
	m_sound.setVolume(20.0f);
	m_sound.play();

}

void SFDrawManager::ClearScreen()
{
	for (std::list<sf::Text*>::iterator i = m_TextList.begin(); i != m_TextList.end(); i++)
	{
		delete* i;
	}
	m_TextList.clear();

	for (std::list<sf::Sprite*>::iterator i = m_SpriteList.begin(); i != m_SpriteList.end(); i++)
	{
		delete* i;
	}
	m_SpriteList.clear();

	if (m_window != nullptr)
	{
		m_window->clear();

		m_window->draw(m_BackgroundSprite);

		m_window->display();
	}
}

void SFDrawManager::DisplayMain()
{

	sf::Text* mainTitle = nullptr;
	sf::Text* subMessage = nullptr;
	sf::Sprite* startIcon = nullptr;
	mainTitle = GenerateText(55, sf::Color::Black, "Welcome to Duel:Gwent Project", "center");
	subMessage = GenerateText(30, sf::Color::Black, "Click icon below to continue!", "center");
	startIcon = GenerateSprite(&m_StartIconTexture, m_StartIconTextureFileName, "center");

	if (m_window != nullptr)
	{
		m_window->clear();

		m_window->draw(m_BackgroundSprite);

		DrawObjectOnView(800.0f, 400.0f, *mainTitle);
		DrawObjectOnView(800.0f, 700.0f, *subMessage);
		DrawObjectOnView(800.0f, 850.0f, *startIcon);

		m_window->display();
	}

	sf::Event event;
	m_window->pollEvent(event);

	while (m_window != nullptr && !isPlayerClickedTarget(event, startIcon))
	{
	}
	ClearScreen();
}

int SFDrawManager::DisplayDeckPick()
{
	ClearScreen();

	sf::Text* mainTitle = nullptr;
	std::vector<sf::Text*> deckTexts;

	mainTitle = GenerateText(55, sf::Color::Black, "Select your Deck!", "center");

	deckTexts.push_back(GenerateText(30, sf::Color::Black, "Hard to survive", "center"));
	deckTexts.push_back(GenerateText(30, sf::Color::Black, "A little bit sweat", "center"));
	deckTexts.push_back(GenerateText(30, sf::Color::Black, "Crush them", "center"));
	deckTexts.push_back(GenerateText(30, sf::Color::Black, "Unknown fate", "center"));

	if (m_window != nullptr)
	{

		DrawObjectOnView(800.0f, 400.0f, *mainTitle);

		DrawObjectOnView(650.0f, 650.0f, *deckTexts[0]);
		DrawObjectOnView(950.0f, 650.0f, *deckTexts[1]);
		DrawObjectOnView(650.0f, 750.0f, *deckTexts[2]);
		DrawObjectOnView(950.0f, 750.0f, *deckTexts[3]);

		m_window->display();
	}

	sf::Event event;
	m_window->pollEvent(event);

	while (m_window != nullptr)
	{
		int deckNumber = GetPlayerClickedText(event, deckTexts);
		if (deckNumber != -1)
		{
			ClearScreen();
			return deckNumber;
		}
		
	}

	return -1;
}


sf::Text* SFDrawManager::GenerateText(int _fontsize, sf::Color _color, std::string _string, std::string _positionString)
{
	sf::Text* newText = new sf::Text();
	m_TextList.push_back(newText);

	newText->setFont(m_font);
	newText->setCharacterSize(_fontsize);
	newText->setFillColor(_color);
	newText->setStyle(sf::Text::Regular);
	newText->setString(_string);
	if (_positionString == "center")
	{
		newText->setOrigin(sf::Vector2f(newText->getLocalBounds().width * 0.5f, newText->getLocalBounds().height * 0.5f));
	}
	else if (_positionString == "left")
	{
		// use default origin for now
	}

	return newText;
}

sf::Sprite* SFDrawManager::GenerateSprite(sf::Texture* _texturePtr, std::string _textureFile, std::string _positionString)
{
	sf::Sprite* newSprite = new sf::Sprite();
	m_SpriteList.push_back(newSprite);
	
	if (_texturePtr->loadFromFile(_textureFile) == false)
	{
		std::cout << "Error loading sprite " << _textureFile << std::endl;
		return nullptr;
	}
	
	newSprite->setTexture(*_texturePtr);

	if (_positionString == "center")
	{
		newSprite->setOrigin(sf::Vector2f(_texturePtr->getSize().x * 0.5f, _texturePtr->getSize().y * 0.5f));
	}
	else if(_positionString == "left")
	{
		// use default origin for now
	}
	
	return newSprite;
}

void SFDrawManager::DrawObjectOnView(float _positionX, float _positionY, sf::Text &_text)
{
	_text.setPosition(_positionX, _positionY);
	m_window->draw(_text);
}

void SFDrawManager::DrawObjectOnView(float _positionX, float _positionY, sf::Sprite &_sprite)
{
	_sprite.setPosition(_positionX, _positionY);
	m_window->draw(_sprite);
}

int SFDrawManager::UpdateGameUIOnPlayerMove(Card* &_cardLastClicked, int _textClicked, Player* _player, Player* _AI, int _roundNumber)
{
	ClearScreen();

	sf::RectangleShape UIlineV(sf::Vector2f(3, 1200));
	UIlineV.setPosition(200.0f, 0.0f);
	UIlineV.setFillColor(sf::Color::Black);

	sf::RectangleShape UIlineH(sf::Vector2f(1600, 3));
	UIlineH.setPosition(0.0f, 450.0f);
	UIlineH.setFillColor(sf::Color::Black);

	sf::Sprite* meleeIcon1 = nullptr;
	sf::Sprite* rangedIcon1 = nullptr;
	sf::Sprite* siegeIcon1 = nullptr;
	sf::Sprite* meleeIcon2 = nullptr;
	sf::Sprite* rangedIcon2 = nullptr;
	sf::Sprite* siegeIcon2 = nullptr;

	sf::Text* AIHandText = nullptr;
	sf::Text* AILifeText = nullptr;
	sf::Text* AIScoreText = nullptr;
	sf::Text* AIDeckText = nullptr;
	sf::Text* AIPassedText = nullptr;

	sf::Text* roundNumberText = nullptr;

	sf::Text* playerHandText = nullptr;
	sf::Text* playerLifeText = nullptr;
	sf::Text* playerScoreText = nullptr;
	sf::Text* playerDeckText = nullptr;
	sf::Text* playerPassedText = nullptr;

	sf::Text* playerPassText = nullptr;
	sf::Text* cardInfoText = nullptr;

	// Load Info
	meleeIcon1 = GenerateSprite(&m_MeleeIconTexture, m_MeleeIconTextureFileName, "center");
	meleeIcon2 = GenerateSprite(&m_MeleeIconTexture, m_MeleeIconTextureFileName, "center");
	rangedIcon1 = GenerateSprite(&m_RangedIconTexture, m_RangedIconTextureFileName, "center");
	rangedIcon2 = GenerateSprite(&m_RangedIconTexture, m_RangedIconTextureFileName, "center");
	siegeIcon1 = GenerateSprite(&m_SiegeIconTexture, m_SiegeIconTextureFileName, "center");
	siegeIcon2 = GenerateSprite(&m_SiegeIconTexture, m_SiegeIconTextureFileName, "center");

	AIHandText = GenerateText(25, sf::Color::Black, "Holds: " + std::to_string(_AI->m_AtHand.size()), "left");
	AILifeText = GenerateText(25, sf::Color::Black, "Life: " + std::to_string(_AI->m_Life), "left");
	AIScoreText = GenerateText(25, sf::Color::Black, "Score: " + std::to_string(_AI->GetTotalPoint()), "left");
	AIDeckText = GenerateText(25, sf::Color::Black, "In Deck: " + std::to_string(_AI->m_Deck.size()) , "left");
	AIPassedText = GenerateText(25, sf::Color::Black, "Passed: " + BoolToString(_AI->m_isPass), "left");

	roundNumberText = GenerateText(35, sf::Color::Black, "Round " + std::to_string(_roundNumber + 1), "left");

	playerHandText = GenerateText(25, sf::Color::Black, "Holds: " + std::to_string(_player->m_AtHand.size()), "left");
	playerLifeText = GenerateText(25, sf::Color::Black, "Life: " + std::to_string(_player->m_Life), "left");
	playerScoreText = GenerateText(25, sf::Color::Black, "Score: " + std::to_string(_player->GetTotalPoint()), "left");
	playerDeckText = GenerateText(25, sf::Color::Black, "In Deck: " + std::to_string(_player->m_Deck.size()), "left");
	playerPassedText = GenerateText(25, sf::Color::Black, "Passed: " + BoolToString(_player->m_isPass), "left");
	playerPassText = GenerateText(25, sf::Color::Black, "Pass!", "left");

	if (_cardLastClicked != nullptr)
	{
		cardInfoText = GenerateText(25, sf::Color::Black, "Info: " + _cardLastClicked->GetCardInfo(), "left");
	}
	else
	{
		cardInfoText = GenerateText(25, sf::Color::Black, "Info: ", "left");
	}

	// Draw on screen
	m_window->draw(UIlineV);
	m_window->draw(UIlineH);

	DrawObjectOnView(1550.0f, 75.0f + 150 * 0, *siegeIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 1, *rangedIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 2, *meleeIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 3, *meleeIcon1);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 4, *rangedIcon1);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 5, *siegeIcon1);

	DrawObjectOnView(25.0f, 45.0f + 90 * 0, *AIHandText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 0.5f, *AILifeText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 1, *AIDeckText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 1.5f, *AIScoreText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 2, *AIPassedText);

	DrawObjectOnView(25.0f, 490.0f, *roundNumberText);

	DrawObjectOnView(25.0f, 45.0f + 90 * 6, *playerScoreText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 6.5f, *playerDeckText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 7, *playerLifeText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 7.5f, *playerHandText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 8, *playerPassedText);

	DrawObjectOnView(25.0f, 45.0f + 90 * 11, *playerPassText);
	DrawObjectOnView(220.0f, 45.0f + 90 * 12, *cardInfoText);

	int i = 0;
	// draw player hand
	for (std::list<Card*>::iterator it = _player->m_AtHand.begin(); it != _player->m_AtHand.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 1035.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	// draw player Melee field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnMeleeField.begin(); it != _player->m_OnMeleeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 525.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw player Ranged field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnRangedField.begin(); it != _player->m_OnRangedField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 675.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw player Siege field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnSiegeField.begin(); it != _player->m_OnSiegeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 825.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	// draw AI Melee field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnMeleeField.begin(); it != _AI->m_OnMeleeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 375.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw AI Ranged field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnRangedField.begin(); it != _AI->m_OnRangedField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 225.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw AI Siege field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnSiegeField.begin(); it != _AI->m_OnSiegeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 75.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	m_window->display();

	// Wait for use click on valid item

	// use for getting clicked item info and display

	sf::Event event;
	m_window->pollEvent(event);

	while (m_window != nullptr)
	{
		m_window->pollEvent(event);
		if (event.type == sf::Event::Closed)
		{
			m_window->close();
			delete m_window;
			m_window = nullptr;
			return -2;
		}
		if (event.type == sf::Event::MouseButtonPressed) 
		{

			Card* cardJustClicked = nullptr;
			cardJustClicked = GetPlayerClickedCard(event, _player->m_AtHand);
		
			if (isPlayerClickedTarget(event, playerPassText))
			{
				std::cout << "Pass clicked" << std::endl;
				ClearScreen();
				return -1;
			}
			else if ( cardJustClicked != nullptr && _cardLastClicked != nullptr && cardJustClicked == _cardLastClicked)
			{
				std::cout << "clicked play a card" << std::endl;
				_cardLastClicked = cardJustClicked;
				ClearScreen();
				return 1;
			}
			else if (cardJustClicked != nullptr && (cardJustClicked != _cardLastClicked || _cardLastClicked == nullptr))
			{
				_cardLastClicked = cardJustClicked;
				ClearScreen();
				return 0;
			}
		}
	
	}

	return -2;
}

void SFDrawManager::UpdateGameUI(Card*& _cardLastClicked, int _textClicked, Player* _player, Player* _AI, int _roundNumber)
{
	ClearScreen();

	sf::RectangleShape UIlineV(sf::Vector2f(3, 1200));
	UIlineV.setPosition(200.0f, 0.0f);
	UIlineV.setFillColor(sf::Color::Black);

	sf::RectangleShape UIlineH(sf::Vector2f(1600, 3));
	UIlineH.setPosition(0.0f, 450.0f);
	UIlineH.setFillColor(sf::Color::Black);

	sf::Sprite* meleeIcon1 = nullptr;
	sf::Sprite* rangedIcon1 = nullptr;
	sf::Sprite* siegeIcon1 = nullptr;
	sf::Sprite* meleeIcon2 = nullptr;
	sf::Sprite* rangedIcon2 = nullptr;
	sf::Sprite* siegeIcon2 = nullptr;

	sf::Text* AIHandText = nullptr;
	sf::Text* AILifeText = nullptr;
	sf::Text* AIScoreText = nullptr;
	sf::Text* AIDeckText = nullptr;
	sf::Text* AIPassedText = nullptr;

	sf::Text* roundNumberText = nullptr;

	sf::Text* playerHandText = nullptr;
	sf::Text* playerLifeText = nullptr;
	sf::Text* playerScoreText = nullptr;
	sf::Text* playerDeckText = nullptr;
	sf::Text* playerPassedText = nullptr;

	sf::Text* playerPassText = nullptr;
	sf::Text* cardInfoText = nullptr;

	// Load Info
	meleeIcon1 = GenerateSprite(&m_MeleeIconTexture, m_MeleeIconTextureFileName, "center");
	meleeIcon2 = GenerateSprite(&m_MeleeIconTexture, m_MeleeIconTextureFileName, "center");
	rangedIcon1 = GenerateSprite(&m_RangedIconTexture, m_RangedIconTextureFileName, "center");
	rangedIcon2 = GenerateSprite(&m_RangedIconTexture, m_RangedIconTextureFileName, "center");
	siegeIcon1 = GenerateSprite(&m_SiegeIconTexture, m_SiegeIconTextureFileName, "center");
	siegeIcon2 = GenerateSprite(&m_SiegeIconTexture, m_SiegeIconTextureFileName, "center");

	AIHandText = GenerateText(25, sf::Color::Black, "Holds: " + std::to_string(_AI->m_AtHand.size()), "left");
	AILifeText = GenerateText(25, sf::Color::Black, "Life: " + std::to_string(_AI->m_Life), "left");
	AIScoreText = GenerateText(25, sf::Color::Black, "Score: " + std::to_string(_AI->GetTotalPoint()), "left");
	AIDeckText = GenerateText(25, sf::Color::Black, "In Deck: " + std::to_string(_AI->m_Deck.size()), "left");
	AIPassedText = GenerateText(25, sf::Color::Black, "Passed: " + BoolToString(_AI->m_isPass), "left");

	roundNumberText = GenerateText(35, sf::Color::Black, "Round " + std::to_string(_roundNumber + 1), "left");

	playerHandText = GenerateText(25, sf::Color::Black, "Holds: " + std::to_string(_player->m_AtHand.size()), "left");
	playerLifeText = GenerateText(25, sf::Color::Black, "Life: " + std::to_string(_player->m_Life), "left");
	playerScoreText = GenerateText(25, sf::Color::Black, "Score: " + std::to_string(_player->GetTotalPoint()), "left");
	playerDeckText = GenerateText(25, sf::Color::Black, "In Deck: " + std::to_string(_player->m_Deck.size()), "left");
	playerPassedText = GenerateText(25, sf::Color::Black, "Passed: " + BoolToString(_player->m_isPass), "left");
	playerPassText = GenerateText(25, sf::Color::Black, "Pass!", "left");

	if (_cardLastClicked != nullptr)
	{
		cardInfoText = GenerateText(25, sf::Color::Black, "Info: " + _cardLastClicked->GetCardInfo(), "left");
	}
	else
	{
		cardInfoText = GenerateText(25, sf::Color::Black, "Info: ", "left");
	}

	// Draw on screen
	m_window->draw(UIlineV);
	m_window->draw(UIlineH);

	DrawObjectOnView(1550.0f, 75.0f + 150 * 0, *siegeIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 1, *rangedIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 2, *meleeIcon2);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 3, *meleeIcon1);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 4, *rangedIcon1);
	DrawObjectOnView(1550.0f, 75.0f + 150 * 5, *siegeIcon1);

	DrawObjectOnView(25.0f, 45.0f + 90 * 0, *AIHandText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 0.5f, *AILifeText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 1, *AIDeckText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 1.5f, *AIScoreText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 2, *AIPassedText);

	DrawObjectOnView(25.0f, 490.0f, *roundNumberText);

	DrawObjectOnView(25.0f, 45.0f + 90 * 6, *playerScoreText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 6.5f, *playerDeckText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 7, *playerLifeText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 7.5f, *playerHandText);
	DrawObjectOnView(25.0f, 45.0f + 90 * 8, *playerPassedText);

	DrawObjectOnView(25.0f, 45.0f + 90 * 11, *playerPassText);
	DrawObjectOnView(220.0f, 45.0f + 90 * 12, *cardInfoText);

	int i = 0;
	// draw player hand
	for (std::list<Card*>::iterator it = _player->m_AtHand.begin(); it != _player->m_AtHand.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 1035.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	// draw player Melee field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnMeleeField.begin(); it != _player->m_OnMeleeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 525.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw player Ranged field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnRangedField.begin(); it != _player->m_OnRangedField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 675.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw player Siege field 
	i = 0;
	for (std::list<Card*>::iterator it = _player->m_OnSiegeField.begin(); it != _player->m_OnSiegeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 825.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	// draw AI Melee field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnMeleeField.begin(); it != _AI->m_OnMeleeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 375.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw AI Ranged field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnRangedField.begin(); it != _AI->m_OnRangedField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 225.0f, *(*it)->m_Sprite);
		i++;
	}
	// draw AI Siege field 
	i = 0;
	for (std::list<Card*>::iterator it = _AI->m_OnSiegeField.begin(); it != _AI->m_OnSiegeField.end(); it++)
	{
		DrawObjectOnView(220.0f + 150.0f * i, 75.0f, *(*it)->m_Sprite);
		i++;
	}
	i = 0;

	m_window->display();
	_cardLastClicked = nullptr;
	ClearScreen();
}

bool SFDrawManager::isPlayerClickedTarget(sf::Event _event, sf::Sprite* _targetSprite)
{
	m_window->pollEvent(_event);
	if (_event.type == sf::Event::MouseButtonPressed)
	{
		if (_event.mouseButton.button == sf::Mouse::Left)
		{
			std::cout << "Left Mouse Button was Pressed" << std::endl;

			auto mousePosition = sf::Mouse::getPosition(*m_window);
			auto mousePositionToWindow = m_window->mapPixelToCoords(mousePosition);

			if (_targetSprite->getGlobalBounds().contains(mousePositionToWindow))
			{
				std::cout << "Button was Pressed" << std::endl;
				return true;
			}
		}
	}

	return false;
}

bool SFDrawManager::isPlayerClickedTarget(sf::Event _event, sf::Text* _targetText)
{
	m_window->pollEvent(_event);
	if (_event.type == sf::Event::MouseButtonPressed)
	{
		if (_event.mouseButton.button == sf::Mouse::Left)
		{
			//std::cout << "Left Mouse Button was Pressed" << std::endl;

			auto mousePosition = sf::Mouse::getPosition(*m_window);
			auto mousePositionToWindow = m_window->mapPixelToCoords(mousePosition);

			if (_targetText->getGlobalBounds().contains(mousePositionToWindow))
			{
				std::cout << "Button was Pressed" << std::endl;
				return true;
			}
		}
	}

	return false;
}

int SFDrawManager::GetPlayerClickedText(sf::Event _event, std::vector<sf::Text*> _targetTexts)
{
	m_window->pollEvent(_event);
	if (_event.type == sf::Event::MouseButtonPressed)
	{
		if (_event.mouseButton.button == sf::Mouse::Left)
		{
			std::cout << "Left Mouse Button was Pressed" << std::endl;

			auto mousePosition = sf::Mouse::getPosition(*m_window);
			auto mousePositionToWindow = m_window->mapPixelToCoords(mousePosition);

			for (int i = 0; i < (int)_targetTexts.size(); i++)
			{
				if (_targetTexts[i]->getGlobalBounds().contains(mousePositionToWindow))
				{
					std::cout << "Text was Pressed" << std::endl;
					return i;
				}
			}
		}
	}

	return -1;
}


Card* SFDrawManager::GetPlayerClickedCard(sf::Event _event, std::list<Card*> _targetCards)
{
	m_window->pollEvent(_event);
	if (_event.type == sf::Event::MouseButtonPressed)
	{
		if (_event.mouseButton.button == sf::Mouse::Left)
		{
			//std::cout << "Left Mouse Button was Pressed" << std::endl;

			auto mousePosition = sf::Mouse::getPosition(*m_window);
			auto mousePositionToWindow = m_window->mapPixelToCoords(mousePosition);

			for (std::list<Card*>::iterator i = _targetCards.begin(); i != _targetCards.end(); i++)
			{
				if ((*i)->m_Sprite->getGlobalBounds().contains(mousePositionToWindow))
				{
					return (*i);
				}
			}
		}
	}

	return nullptr;
}

std::string SFDrawManager::BoolToString(bool _isTrue)
{
	return _isTrue ? "Yes" : "No";
}

void SFDrawManager::LoadGameOverScreen(std::string _message)
{
	ClearScreen();

	sf::Text* messageText = nullptr;
	sf::Text* exitText = nullptr;

	messageText = GenerateText(55, sf::Color::Black, _message, "center");
	exitText = GenerateText(55, sf::Color::Black, "Click me to Exit", "center");

	DrawObjectOnView(800.0f, 400.0f, *messageText);
	DrawObjectOnView(800.0f, 850.0f, *exitText);

	m_window->display();

	while (true)
	{
		sf::Event event;

		while (m_window != nullptr && m_window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				m_window->close();
				delete m_window;
				m_window = nullptr;
				break;

			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					auto mousePosition = sf::Mouse::getPosition(*m_window);
					auto mousePositionToWindow = m_window->mapPixelToCoords(mousePosition);
					if (exitText->getGlobalBounds().contains(mousePositionToWindow))
					{
						m_window->close();
						delete m_window;
						m_window = nullptr;
						return;
					}
				}
				break;
			default:
				break;
			}
		}
	}
}
