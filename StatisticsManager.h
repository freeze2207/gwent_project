#pragma once
#ifndef STATISTICS_H
#define STATISTICS_H

#include <string>
#include <iostream>
#include <map>
#include "sqlite3.h"

class StatisticsManager
{
private:
	int m_PlayerWins = 0;
	int m_AIWins = 0;
	int m_TotalGames = 0;
	int m_ThreeRoundsGame = 0;
	int m_CardPlayedthisRound = 0;
	std::string m_DBpath = "MyGameStats.db";
	std::map<std::string, int>* m_AlltimeResult = nullptr;

public:

	StatisticsManager(std::map<std::string, int>* _readDataptr);
	~StatisticsManager();
	void Initialize();
	
	sqlite3* OpenDB();
	bool UpdateDBAllTime();
	bool InsertDBRound();

	void AddPlayerWins();
	void AddAIWins();
	void AddTotalGame();
	void AddThreeRoundsGame();
	void GetAllCardsPlayedThisRound(int _sum);
};

#endif // !STATISTICS_H
