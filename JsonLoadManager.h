#pragma once
#ifndef JSONLOADMANAGER_H
#define JSONLOADMANAGER_H

#include <iostream>
#include <fstream> 
#include <list>
#include "json.hpp"

class Card;

class JsonLoadManager
{
public:
private:
	nlohmann::json m_SetJson;
	std::list<Card*>* m_listOfCards = nullptr;

public:
	JsonLoadManager();
	~JsonLoadManager();

	nlohmann::json LoadFileToJson(std::string _filePath);
	std::list<Card*>* JsonToListCard(nlohmann::json _object);

	std::list<Card*>* JsonFileToListCard(std::string _filePath);
};

#endif // !JSONLOADMANAGER_H
