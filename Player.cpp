#include "Player.h"
#include "Card.h"

Player::Player()
{
}

Player::~Player()
{
}

void Player::DrawACard()
{
    if (m_Deck.size() == 0)
    {
        return;
    }
    Card* cardOnTop = m_Deck.back();
    m_AtHand.push_back(cardOnTop);
    m_Deck.pop_back();
}


const int Player::GetTotalPoint()
{

    m_MeleePoint = GetRowTotalPoint(m_OnMeleeField, m_MeleeEffects);
    m_RangedPoint = GetRowTotalPoint(m_OnRangedField, m_RangedEffects);
    m_SiegePoint = GetRowTotalPoint(m_OnSiegeField, m_SiegeEffects);

    return m_MeleePoint + m_RangedPoint + m_SiegePoint;
}

int Player::GetRowTotalPoint(std::list<Card*> _list, std::list<std::string> _effectList)
{
    // apply effects to each card in row
    std::list<std::string> modifierList;
    for (std::list<std::string>::iterator it_j = _effectList.begin(); it_j != _effectList.end(); it_j++)
    {
        modifierList.push_back(*it_j);
    }
    // use value assign not push back avoid multicast for effect 
    for (std::list<Card*>::iterator it_i = _list.begin(); it_i != _list.end(); it_i++)
    {
        (*it_i)->m_Modifiers = modifierList; 
    }

    int totalPoint = 0;
    // calculate every card actual point
    for (std::list<Card*>::iterator it = _list.begin(); it != _list.end(); it++)
    {
        totalPoint += (*it)->GetCardActualPoint();
    }

    return totalPoint;
}
