#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "GamePlay.h"
#include "Player.h"
#include "SFDrawManager.h"
#include "JsonLoadManager.h"
#include "StatisticsManager.h"
#include "Card.h"

GamePlay::GamePlay(std::map<std::string, int>* _readDataptr)
{
    m_ReadDataptr = _readDataptr;
}

GamePlay::~GamePlay()
{
    delete m_cardLastClicked;
    delete m_Player;
    delete m_AI;
    delete m_DrawManager;
    delete m_JsonLoadManager;
    delete m_statManager;

}

void GamePlay::Initialize()
{
    m_DrawManager = new SFDrawManager();
    m_JsonLoadManager = new JsonLoadManager();
    m_statManager = new StatisticsManager(m_ReadDataptr);

    m_statManager->Initialize();
    
    // Load 4 type of decks in game init
    m_WeakDeck = m_JsonLoadManager->JsonFileToListCard(m_WeakDeckFilePath);
    m_MidDeck = m_JsonLoadManager->JsonFileToListCard(m_MidDeckFilePath);
    m_OPDeck = m_JsonLoadManager->JsonFileToListCard(m_OPDeckFilePath);

    std::list<Card*>* newRandomPtr = m_JsonLoadManager->JsonFileToListCard(m_RandomDeckFilePath);
    m_RandomDeck = GenerateRandomDeck(newRandomPtr);
    
    m_DrawManager->Initialize();

    // Display Main and wait for user correct input
    m_DrawManager->DisplayMain();
}

// generate a Random deck by shuffle the total card bank and take first 15!
std::list<Card*>* GamePlay::GenerateRandomDeck(std::list<Card*>* _deckPtr)
{
    std::list<Card*>* newDeckPtr = new std::list<Card*>();
    ShuffleDecks(*_deckPtr);
    for (int i = 0; i < 15; i++)
    {   
        std::list<Card*>::iterator it = _deckPtr->begin();
        std::advance(it, i);
        newDeckPtr->push_back(*it);
    }

    return newDeckPtr;
}

// random to toss the coin who goes first at first round
bool GamePlay::DecideWhoGoFirst()
{
    std::random_device device;
    std::mt19937 randomGenerator(device());
    std::uniform_int_distribution<std::mt19937::result_type> dist2(0, 1);

    return dist2(randomGenerator);
}

// AI fate generator
int GamePlay::AIDeckPick()
{
    std::random_device device;
    std::mt19937 randomGenerator(device());
    std::uniform_int_distribution<std::mt19937::result_type> dist4(0, 3);

    return dist4(randomGenerator);
}

// Players pick their deck and load to their deck
void GamePlay::PlayerPickDeck()
{
    // Make a copy to avoid same address issue
    std::list<Card*>* newDeckptr = GetDeck(m_DrawManager->DisplayDeckPick());
    std::list<Card*> copyOfNewDeck;
    for (std::list<Card*>::iterator it = (*newDeckptr).begin(); it != (*newDeckptr).end(); it++)
    {
        Card* newCard = new Card((**it).GetName(), (**it).GetRow(), (**it).GetPoint(), (**it).GetAbilities()
            , (**it).GetImage(), (**it).GetDescription(), (**it).GetRarity());
        copyOfNewDeck.push_back(newCard);
    }

    m_Player->m_Deck = copyOfNewDeck;


    std::list<Card*>* newDeckptr0 = GetDeck(AIDeckPick());
    std::list<Card*> copyOfNewDeck0;
    for (std::list<Card*>::iterator it = (*newDeckptr0).begin(); it != (*newDeckptr0).end(); it++)
    {
        Card* newCard0 = new Card((**it).GetName(), (**it).GetRow(), (**it).GetPoint(), (**it).GetAbilities()
            , (**it).GetImage(), (**it).GetDescription(), (**it).GetRarity());
        copyOfNewDeck0.push_back(newCard0);
    }

    m_AI->m_Deck = copyOfNewDeck0;

}

// Get deck based on 0-3 number
std::list<Card*>* GamePlay::GetDeck(int _number)    
{
    switch (_number)
    {
    case 0:
        return m_WeakDeck;
        break;
    case 1:
        return m_MidDeck;
        break;
    case 2:
        return m_OPDeck;
        break;
    case 3:
        return m_RandomDeck;
        break;
    default:
        std::cout << "Invalid deck number" << std::endl;
        return nullptr;
        break;
    }
}

void GamePlay::HandOutInitCards()
{
    // Player and AI both draw 4 cards
    for (int i = 0; i < 4; i++)
    {
        m_Player->DrawACard();
        m_AI->DrawACard();
    }

    // check if
    if (m_Player->m_AtHand.size() != 4 || m_AI->m_AtHand.size() != 4)
    {
        std::cout << "Error in Hand out cards" << std::endl;
        return;
    }
}

void GamePlay::GameStart()
{
    // proper init 
    m_Player = new Player();
    m_AI = new Player();
    m_AI->m_isAI = true;

    PlayerPickDeck();

    ShuffleDecks(m_Player->m_Deck);
    ShuffleDecks(m_AI->m_Deck);

    HandOutInitCards();


    this->m_isPlayerTurn = DecideWhoGoFirst();

    // no one's life is 0 keep fighting 
    while (this->m_Player->m_Life > 0 && this->m_AI->m_Life > 0)
    {
        RoundStart();
        RoundGoing();
        RoundEnd();
    }
}


void GamePlay::ShuffleDecks(std::list<Card*> &_deck)
{
    std::vector<Card*> tempVector;
    for (Card* card : _deck)
    {
        tempVector.push_back(card);
    }

    std::srand(unsigned(std::time(0)));
    std::random_shuffle(tempVector.begin(), tempVector.end());

    _deck.clear();
    for (Card* card : tempVector)
    {
        _deck.push_back(card);
    }

}

//reset every game
void GamePlay::RoundStart()
{
    // what need to reset every round ?
    
    RoundReset();

}

void GamePlay::RoundReset()
{
    m_Player->m_isPass = false;
    m_Player->m_OnField.clear();
    m_Player->m_OnMeleeField.clear();
    m_Player->m_OnRangedField.clear();
    m_Player->m_OnSiegeField.clear();

    m_AI->m_isPass = false;
    m_AI->m_OnField.clear();
    m_AI->m_OnMeleeField.clear();
    m_AI->m_OnRangedField.clear();
    m_AI->m_OnSiegeField.clear();
}

void GamePlay::RoundGoing()
{
    // continue play cards till both pass
    while ( !this->m_AI->m_isPass || !this->m_Player->m_isPass)
    {
        // Lets take turns to move
        if (m_isPlayerTurn)
        {
            if (m_Player->m_AtHand.size() == 0)
            {
                m_Player->m_isPass = true;

            }
            else
            {
                // player move and UI binds together
                // update the Draw based on field[] hand[] deck[]
                int state_result = m_DrawManager->UpdateGameUIOnPlayerMove(m_cardLastClicked, m_textUserClicked, m_Player, m_AI, m_RoundNumber);

                while (state_result == 0)
                {
                    state_result = m_DrawManager->UpdateGameUIOnPlayerMove(m_cardLastClicked, m_textUserClicked, m_Player, m_AI, m_RoundNumber);
                }
                if (state_result == -2)
                {
                    std::cout << "Error updating GameUIInfo" << std::endl;
                    return;
                }
                else if (state_result == -1)
                {

                    m_Player->m_isPass = true;
                }
                else if (state_result == 1)
                {
                    std::cout << "play a card" << std::endl;
                    PlayACard(m_cardLastClicked, m_Player);
                    m_Player->DrawACard();

                }
            }

            m_isPlayerTurn = !m_isPlayerTurn;
        }
        else
        {
            if (m_AI->m_AtHand.size() == 0 )
            {
                m_AI->m_isPass = true;
                continue;
            }
            // AI move logic

            AIMoves();

            m_DrawManager->UpdateGameUI(m_cardLastClicked, m_textUserClicked, m_Player, m_AI, m_RoundNumber);
            m_isPlayerTurn = !m_isPlayerTurn;
        }

    }
}

void GamePlay::AIMoves()
{
    if (m_AI->m_isPass)
    {
        return;
    }
    else
    {
        // played 6 cards
        if (m_AI->m_OnField.size() == 6)
        {
            m_AI->m_isPass = true;
            return;
        }
        // winner by 5 or losing by 7
        if (m_Player->GetTotalPoint() - m_AI->GetTotalPoint() > 7 || m_AI->GetTotalPoint() - m_Player->GetTotalPoint() > 5)
        {
            m_AI->m_isPass = true;
            return;
        }
        // play a random card
        srand(time(NULL));
        int i = std::rand() / ((RAND_MAX + 1u) / m_AI->m_AtHand.size());
        std::list<Card*>::iterator it = m_AI->m_AtHand.begin();
        std::advance(it, i);
        PlayACard(*it, m_AI);
        m_AI->DrawACard();

        // played 6 cards
        if (m_AI->m_OnField.size() == 6)
        {
            m_AI->m_isPass = true;

        }
        // winner by 5 or losing by 7
        if (m_Player->GetTotalPoint() - m_AI->GetTotalPoint() > 7 || m_AI->GetTotalPoint() - m_Player->GetTotalPoint() > 5)
        {
            m_AI->m_isPass = true;

        }
    }
    
}

Player* GamePlay::JudgeResult()
{
    int playerScore = this->m_Player->GetTotalPoint();
    int AIScore = this->m_AI->GetTotalPoint();
    if (playerScore > AIScore)
    {
        return m_AI;
    }
    else if (playerScore < AIScore)
    {
        return m_Player;
    }
    else
    {
        return nullptr;
    }

}

void GamePlay::RoundEnd()
{
    Player* loser = JudgeResult();
    
    if (loser != nullptr)
    {
        loser->m_Life--;
        // winner go first next round
        m_isPlayerTurn = loser->m_isAI ? true : false;
    }
    else
    {
        this->m_Player->m_Life--;
        this->m_AI->m_Life--;
        // draw will still random who first
        m_isPlayerTurn = DecideWhoGoFirst();
    }

    // Record Round Data
    m_statManager->GetAllCardsPlayedThisRound(m_Player->m_OnField.size() + m_AI->m_OnField.size());
    if (!m_statManager->InsertDBRound())
    {
        std::cout << "Error insert into Round data" << std::endl;
    }

    m_RoundNumber++;
}

void GamePlay::PlayACard(Card* _card, Player* _player)
{
    //Get rows and effects
    std::string row = "";
    row = _card->GetRow();
    if (row == "Melee")
    {
        _player->m_OnMeleeField.push_back(_card);
        CardEffectsIntoField(_card, &_player->m_MeleeEffects);
    }
    else if (row == "Ranged")
    {
        _player->m_OnRangedField.push_back(_card);
        CardEffectsIntoField(_card, &_player->m_RangedEffects);
    }
    else if (row == "Siege")
    {
        _player->m_OnSiegeField.push_back(_card);
        CardEffectsIntoField(_card, &_player->m_SiegeEffects);
    }
    else
    {
        std::cout << "Error on Play card, Wrong row type" << std::endl;
        return;
    }


    _player->m_OnField.push_back(_card);
    _player->m_AtHand.remove(_card);

    return;
}


void GamePlay::CardEffectsIntoField(Card* _card, std::list<std::string>* _effectList)
{
    std::list<std::string> abilities = _card->GetAbilities();
    for (std::list<std::string>::iterator it = abilities.begin(); it != abilities.end(); it++)
    {
        _effectList->push_back(*it);
    }
}

// Game over message
void GamePlay::GameEnd()
{
    // Record Game win/lose result
    if (m_Player->m_Life != 0)
    {
        m_statManager->AddPlayerWins();
    }
    if (m_AI->m_Life != 0)
    {
        m_statManager->AddAIWins();
    }
    if (m_RoundNumber == 3)
    {
        m_statManager->AddThreeRoundsGame();
    }
    m_statManager->AddTotalGame();

    // Update Database
    if (!m_statManager->UpdateDBAllTime())
    {
        std::cout << "error updating alltime data" << std::endl;
    }

    std::string message = m_Player->m_Life == 0 ? "You have lost the game" : "You beat your opponent hard";
    m_DrawManager->LoadGameOverScreen(message);
}


