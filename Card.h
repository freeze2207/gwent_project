#pragma once
#ifndef CARD_H
#define CARD_H
#include <iostream>
#include <string>
#include <list>

#include "SFML/Graphics.hpp"

class Card
{
	public: 
		std::list<std::string> m_Modifiers;
		sf::Sprite* m_Sprite = nullptr;
		sf::Texture* m_Texture = nullptr;

	private:
		std::string m_Name = "";
		std::string m_Row = "";
		int m_OriginalPoint = 0;
		std::list<std::string> m_Abilities;
		std::string m_ImagePath = "";
		std::string m_Description = "";
		std::string m_Rarity = "";



	public:
		Card( );
		Card(std::string _name, std::string _row, int _point, std::list<std::string> _ability, std::string _image, std::string _description, std::string _rarity);
		~Card();
		std::string GetName();
		std::string GetRow();
		int GetPoint();
		std::list<std::string> GetAbilities();
		std::string GetImage();
		std::string GetDescription();
		std::string GetRarity();
		std::list<std::string> GetModifier();
		sf::Sprite* GetSprite();
		sf::Texture* GetTexture();

		std::string GetCardInfo();

		int GetCardActualPoint();
};

#endif // !CARD_H
