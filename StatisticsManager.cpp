#include "StatisticsManager.h"


StatisticsManager::StatisticsManager(std::map<std::string, int>* _readDataptr)
{
	m_AlltimeResult = _readDataptr;
}

StatisticsManager::~StatisticsManager()
{

}

void StatisticsManager::Initialize()
{
	m_PlayerWins = m_AlltimeResult->at("PlayerWins");
	m_AIWins = m_AlltimeResult->at("AIWins");
	m_TotalGames = m_AlltimeResult->at("TotalGame");
	m_ThreeRoundsGame = m_AlltimeResult->at("ThreeRoundGame");

}

sqlite3* StatisticsManager::OpenDB()
{
	int result = 0;
	sqlite3* db = nullptr;

	result = sqlite3_open(m_DBpath.c_str(), &db);
	if (result)
	{
		std::cout << "Can not open the db: " << sqlite3_errmsg(db) << std::endl;
		return nullptr;
	}

	return db;
}

bool StatisticsManager::UpdateDBAllTime()
{
	sqlite3* db = OpenDB();

	char* errMsg = nullptr;
	int result = 0;
	sqlite3_stmt* sqlStmt = nullptr;

	std::string sql = "UPDATE AllTimeData SET PlayerWins = @PlayerWins, AIWins = @AIWins, TotalGame = @TotalGame, ThreeRoundGame = @ThreeRoundGame WHERE id = 1;";

	result = sqlite3_prepare_v2(db, sql.c_str(), -1, &sqlStmt, nullptr);
	if (result == SQLITE_OK)
	{
		int idx = 0;
		idx = sqlite3_bind_parameter_index(sqlStmt, "@PlayerWins");
		sqlite3_bind_int(sqlStmt, idx, m_PlayerWins);

		idx = sqlite3_bind_parameter_index(sqlStmt, "@AIWins");
		sqlite3_bind_int(sqlStmt, idx, m_AIWins);

		idx = sqlite3_bind_parameter_index(sqlStmt, "@TotalGame");
		sqlite3_bind_int(sqlStmt, idx, m_TotalGames);

		idx = sqlite3_bind_parameter_index(sqlStmt, "@ThreeRoundGame");
		sqlite3_bind_int(sqlStmt, idx, m_ThreeRoundsGame);

	}
	else
	{
		std::cout << "Failed to prepare statement: " << sql.c_str() << "Error Msg: " << sqlite3_errmsg(db) << std::endl;
		return false;
	}

	int step = sqlite3_step(sqlStmt);
	if (step != SQLITE_DONE)
	{
		std::cout << "Issue executing the preparing statement " << std::endl;
	}
	sqlite3_finalize(sqlStmt);

	sqlite3_close(db);

	db = nullptr;

	return (step == SQLITE_DONE);

}

bool StatisticsManager::InsertDBRound()
{
	sqlite3* db = OpenDB();
	char* errMsg = nullptr;
	int result = 0;
	sqlite3_stmt* sqlStmt = nullptr;

	std::string sql = "INSERT INTO RoundData(CardsPlayed) " \
		"VALUES (@CardsPlayed);";

	result = sqlite3_prepare_v2(db, sql.c_str(), -1, &sqlStmt, nullptr);
	if (result == SQLITE_OK)
	{
		int idx = 0;
		idx = sqlite3_bind_parameter_index(sqlStmt, "@CardsPlayed");
		sqlite3_bind_int(sqlStmt, idx, m_CardPlayedthisRound);

	}
	else
	{
		std::cout << "Failed to prepare statement: " << sql.c_str() << "Error Msg: " << sqlite3_errmsg(db) << std::endl;
		return false;
	}

	int step = sqlite3_step(sqlStmt);
	if (step != SQLITE_DONE)
	{
		std::cout << "Issue executing the preparing statement " << std::endl;
	}
	sqlite3_finalize(sqlStmt);

	sqlite3_close(db);

	db = nullptr;

	return (step == SQLITE_DONE);

}

void StatisticsManager::AddPlayerWins()
{
	m_PlayerWins++;
}

void StatisticsManager::AddAIWins()
{
	m_AIWins++;
}

void StatisticsManager::AddTotalGame()
{
	m_TotalGames++;
}

void StatisticsManager::AddThreeRoundsGame()
{
	m_ThreeRoundsGame++;
}

void StatisticsManager::GetAllCardsPlayedThisRound(int _sum)
{
	m_CardPlayedthisRound = _sum;
}
