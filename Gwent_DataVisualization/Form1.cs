﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gwent_DataVisualization
{
    public partial class Form1 : Form
    {
        //private static string DatabasePath = "./../../../MyGameStats.db";
        private static string DatabasePath = "";

        //All time data
        private int m_PlayerWins = 0;
        private int m_AIWins = 0;
        private int m_TotalGames = 0;
        private int m_ThreeRoundsGame = 0;

        // Round Data
        List<int> m_CardPlayedList = new List<int>();

        public Form1()
        {
            InitializeComponent();

        }


        private void SQLite_ReadData()
        {
            DataTable allTimeDataTable = new DataTable();
            DataTable roundDataTable = new DataTable();

            try
            {
                using (SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + DatabasePath))
                {
                    dbConnection.Open();

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT * from AllTimeData", dbConnection);
                    adapter.Fill(allTimeDataTable);

                    adapter = new SQLiteDataAdapter("SELECT * from RoundData", dbConnection);
                    adapter.Fill(roundDataTable);


                    dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Reading data: " + ex.Message);
            }

            foreach (DataRow row in allTimeDataTable.Rows)
            {
                m_PlayerWins = Convert.ToInt32(row["PlayerWins"]);
                m_AIWins = Convert.ToInt32(row["AIWins"]);
                m_TotalGames = Convert.ToInt32(row["TotalGame"]);
                m_ThreeRoundsGame = Convert.ToInt32(row["ThreeRoundGame"]);
            }

            foreach (DataRow row in roundDataTable.Rows)
            {
                m_CardPlayedList.Add(Convert.ToInt32(row["CardsPlayed"]));
            }
 
        }

        // Display data 
        private void DisplayData()
        {
            alltimechart.Series["Wins"].Points.AddXY("Player", m_PlayerWins);
            alltimechart.Series["Wins"].Points.AddXY("AI", m_AIWins);

            piechart.Series["ThreeRound"].Points.AddXY("Three-Rounds game", m_ThreeRoundsGame);
            piechart.Series["ThreeRound"].Points.AddXY("Rest Games", m_TotalGames - m_ThreeRoundsGame);

            foreach(int number in m_CardPlayedList)
            {
                roundchart.Series["CardsPlayed"].Points.Add(number);
            }
        }

        private void locateDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Select deck files to load
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Select DB file to load:",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "db",
                Filter = "db files (*.db)|*.db",
                FilterIndex = 1,
                RestoreDirectory = true,

            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Keep a reference for further use
                DatabasePath = openFileDialog.FileName;
                
            }

            SQLite_ReadData();
            DisplayData();
        }
    }
}
