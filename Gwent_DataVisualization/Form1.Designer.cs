﻿namespace Gwent_DataVisualization
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.alltimechart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.alltimeLabel = new System.Windows.Forms.Label();
            this.roundchart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.piechart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.roundLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.locateDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.alltimechart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundchart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piechart)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // alltimechart
            // 
            chartArea1.Name = "ChartArea1";
            this.alltimechart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.alltimechart.Legends.Add(legend1);
            this.alltimechart.Location = new System.Drawing.Point(12, 68);
            this.alltimechart.Name = "alltimechart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Wins";
            this.alltimechart.Series.Add(series1);
            this.alltimechart.Size = new System.Drawing.Size(300, 165);
            this.alltimechart.TabIndex = 0;
            this.alltimechart.Text = "Wins Chart";
            title1.Name = "Title1";
            title1.Text = "All-time Wins";
            this.alltimechart.Titles.Add(title1);
            // 
            // alltimeLabel
            // 
            this.alltimeLabel.AutoSize = true;
            this.alltimeLabel.Location = new System.Drawing.Point(36, 42);
            this.alltimeLabel.Name = "alltimeLabel";
            this.alltimeLabel.Size = new System.Drawing.Size(70, 13);
            this.alltimeLabel.TabIndex = 1;
            this.alltimeLabel.Text = "All-Time Data";
            // 
            // roundchart
            // 
            chartArea2.Name = "ChartArea1";
            this.roundchart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.roundchart.Legends.Add(legend2);
            this.roundchart.Location = new System.Drawing.Point(90, 272);
            this.roundchart.Name = "roundchart";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "CardsPlayed";
            this.roundchart.Series.Add(series2);
            this.roundchart.Size = new System.Drawing.Size(557, 165);
            this.roundchart.TabIndex = 2;
            this.roundchart.Text = "chart2";
            title2.Name = "Title1";
            title2.Text = "Cards played Each Round";
            this.roundchart.Titles.Add(title2);
            // 
            // piechart
            // 
            chartArea3.Name = "ChartArea1";
            this.piechart.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.piechart.Legends.Add(legend3);
            this.piechart.Location = new System.Drawing.Point(382, 68);
            this.piechart.Name = "piechart";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.Legend = "Legend1";
            series3.Name = "ThreeRound";
            this.piechart.Series.Add(series3);
            this.piechart.Size = new System.Drawing.Size(296, 165);
            this.piechart.TabIndex = 3;
            this.piechart.Text = "chart1";
            title3.Name = "Title1";
            title3.Text = "Three-Rounds Game vs Total Game";
            this.piechart.Titles.Add(title3);
            // 
            // roundLabel
            // 
            this.roundLabel.AutoSize = true;
            this.roundLabel.Location = new System.Drawing.Point(36, 236);
            this.roundLabel.Name = "roundLabel";
            this.roundLabel.Size = new System.Drawing.Size(70, 13);
            this.roundLabel.TabIndex = 4;
            this.roundLabel.Text = "Rounds Data";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.locateDatabaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(719, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // locateDatabaseToolStripMenuItem
            // 
            this.locateDatabaseToolStripMenuItem.Name = "locateDatabaseToolStripMenuItem";
            this.locateDatabaseToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.locateDatabaseToolStripMenuItem.Text = "Locate Database";
            this.locateDatabaseToolStripMenuItem.Click += new System.EventHandler(this.locateDatabaseToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 449);
            this.Controls.Add(this.roundLabel);
            this.Controls.Add(this.piechart);
            this.Controls.Add(this.roundchart);
            this.Controls.Add(this.alltimeLabel);
            this.Controls.Add(this.alltimechart);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Gwent Stat Visual";
            ((System.ComponentModel.ISupportInitialize)(this.alltimechart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundchart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piechart)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart alltimechart;
        private System.Windows.Forms.Label alltimeLabel;
        private System.Windows.Forms.DataVisualization.Charting.Chart roundchart;
        private System.Windows.Forms.DataVisualization.Charting.Chart piechart;
        private System.Windows.Forms.Label roundLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem locateDatabaseToolStripMenuItem;
    }
}

