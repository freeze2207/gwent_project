#pragma once
#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include <list>
#include <string>
#include <map>

class Player;
class SFDrawManager;
class Card;
class JsonLoadManager;
class StatisticsManager;

class GamePlay
{
public:
	int m_RoundNumber = 0;

	Card* m_cardLastClicked = nullptr;	// Track User click between UI and game 
	int m_textUserClicked = -1;		// Track User click between UI and game 

private:
	Player* m_Player = nullptr;
	Player* m_AI = nullptr;
	bool m_isPlayerTurn = false;
	SFDrawManager* m_DrawManager = nullptr;
	JsonLoadManager* m_JsonLoadManager = nullptr;
	StatisticsManager* m_statManager = nullptr;
	std::map<std::string, int>* m_ReadDataptr = nullptr;

	std::string m_WeakDeckFilePath = "Decks/weak_deck.json";
	std::string m_MidDeckFilePath = "Decks/mid_deck.json";
	std::string m_OPDeckFilePath = "Decks/op_deck.json";
	std::string m_RandomDeckFilePath = "Decks/bank_totalcards.json";

	std::list<Card*>* m_WeakDeck = nullptr;
	std::list<Card*>* m_MidDeck = nullptr;
	std::list<Card*>* m_OPDeck = nullptr;
	std::list<Card*>* m_RandomDeck = nullptr;

public:
	GamePlay(std::map<std::string, int>* _readDataptr);
	~GamePlay();

	void Initialize();

	std::list<Card*>* GenerateRandomDeck(std::list<Card*>* _deckPtr);

	bool DecideWhoGoFirst();

	int AIDeckPick();

	void PlayerPickDeck();

	std::list<Card*>* GetDeck(int _number);

	void HandOutInitCards();

	void GameStart();

	void RoundStart();

	void RoundReset();

	void ShuffleDecks(std::list<Card*> &_deck);

	void RoundGoing();

	void AIMoves();

	Player* JudgeResult();

	void RoundEnd();

	void PlayACard(Card* _card, Player* _player);

	void CardEffectsIntoField(Card* _card, std::list<std::string>* _effectList);

	void GameEnd();
};

#endif // !GAMEPLAY_H
