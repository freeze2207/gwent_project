#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <list>
#include <string>
#include <algorithm>

class Card;

class Player
{
	public:
		int m_Life = 2;
		bool m_isPass = false;
		bool m_isAI = false;
		int m_MeleePoint = 0;
		int m_RangedPoint = 0;
		int m_SiegePoint = 0;
		std::list<std::string> m_MeleeEffects;
		std::list<std::string> m_RangedEffects;
		std::list<std::string> m_SiegeEffects;
		std::list<Card*> m_Deck;
		std::list<Card*> m_AtHand;
		std::list<Card*> m_OnField;

		std::list<Card*> m_OnMeleeField;
		std::list<Card*> m_OnRangedField;
		std::list<Card*> m_OnSiegeField;
		

	private: 
		int m_PlayerNumber = 0;


	public:
		Player();
		~Player();

		void DrawACard();

		const int GetTotalPoint();

		int GetRowTotalPoint(std::list<Card*> _list, std::list<std::string> _effectList);
};


#endif // !PLAYER_H