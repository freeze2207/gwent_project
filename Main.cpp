#include <iostream>
#include <cassert>
#include <map>
#include "GamePlay.h"
#include "StatisticsManager.h"
#include "sqlite3.h"


// Not able to fix the static callback issue placing data read in main
static std::map<std::string, int>* DBReadResult = new std::map<std::string, int>();

static int GetReadDataCallBack(void* _discard, int _colCount, char** _colValue, char** _colName)
{
	for (int i = 1; i < _colCount; i++)
	{
		//std::cout << _colName[i] << " " << _colValue[i] << std::endl;
		DBReadResult->emplace(_colName[i], atoi(_colValue[i]));
	}

	return 0;
}

sqlite3* OpenDB()
{
	int result = 0;
	sqlite3* db = nullptr;

	result = sqlite3_open("MyGameStats.db", &db);
	if (result)
	{
		std::cout << "Can not open the db: " << sqlite3_errmsg(db) << std::endl;
		return nullptr;
	}

	return db;
}

bool Read(sqlite3* db)
{
	char* errMsg = nullptr;
	int result = 0;

	std::string sql = "SELECT * FROM AllTimeData";

	result = sqlite3_exec(db, sql.c_str(), GetReadDataCallBack, nullptr, &errMsg);
	if (result)
	{
		std::cout << "SQL Error: " << errMsg << std::endl;
		return false;
	}

	return true;
}

int main()
{
	sqlite3* db = nullptr;
	bool result = false;

	db = OpenDB();

	if (db != nullptr)
	{
		result = Read(db);
		sqlite3_close(db);
		db = nullptr;
	}
	else
	{
		std::cout << "Failed to open database" << std::endl;
	}

	GamePlay* game = new GamePlay(DBReadResult);

	game->Initialize();

	game->GameStart();
	
	game->GameEnd();

	delete game;
	delete DBReadResult;
}
