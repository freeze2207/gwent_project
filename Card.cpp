#include <algorithm>
#include "Card.h"

Card::Card()
{
}

Card::Card(std::string _name, std::string _row, int _point, std::list<std::string> _ability, std::string _image, std::string _description, std::string _rarity)
	: m_Name(_name), m_Row(_row), m_OriginalPoint(_point), m_Abilities(_ability), m_ImagePath(_image), m_Description(_description), m_Rarity(_rarity)
{
	sf::Sprite* spritePtr = new sf::Sprite();
	sf::Texture* texturePtr = new sf::Texture();
	if (texturePtr->loadFromFile("CardImages/" + m_ImagePath) == false)
	{
		std::cout << "Error loading the Texture"<< m_ImagePath << std::endl;
		return;
	}
	spritePtr->setTexture(*texturePtr);

	m_Sprite = spritePtr;
	m_Texture = texturePtr;
}

Card::~Card()
{
	delete m_Sprite;
	delete m_Texture;
}

std::string Card::GetName()
{
	return m_Name;
}

std::string Card::GetRow()
{
	return m_Row;
}

int Card::GetPoint()
{
	return m_OriginalPoint;
}

std::string Card::GetCardInfo()
{
	std::string abilityList = "";
	if (m_Abilities.size() > 0)
	{
		for (std::string ability : m_Abilities)
		{
			abilityList += ability + "; ";
		}
	}
	else
	{
		abilityList = "None";
	}
	
	return m_Name + ": " + "Row: " + m_Row + " Point: " + 
		std::to_string(m_OriginalPoint) + " Rarity: " + m_Rarity + " Ability: " + abilityList
		+ "\n" + m_Description;
}

std::list<std::string> Card::GetAbilities()
{
	return m_Abilities;
}

std::string Card::GetImage()
{
	return m_ImagePath;
}

std::string Card::GetDescription()
{
	return m_Description;
}

std::string Card::GetRarity()
{
	return m_Rarity;
}

std::list<std::string> Card::GetModifier()
{
	return m_Modifiers;
}

sf::Sprite* Card::GetSprite()
{
	return m_Sprite;
}

sf::Texture* Card::GetTexture()
{
	return m_Texture;
}

int Card::GetCardActualPoint()
{
	int point = m_OriginalPoint;

	// check for Hero effect
	if (std::find(m_Abilities.begin(), m_Abilities.end(), "Hero") != m_Abilities.end())
	{
		m_Modifiers.clear();
	}
	// check for Double-Edge
	if (std::find(m_Abilities.begin(), m_Abilities.end(), "Double-Edge") != m_Abilities.end())
	{
		m_Modifiers.remove("Double-Edge");
		point = m_OriginalPoint * 2;
	}
	// check for Morale Boost effect
	int moraleBoostCount = 0;
	int doubleEdgeCount = 0;
	for (std::list<std::string>::iterator it = m_Modifiers.begin(); it != m_Modifiers.end(); it++)
	{
		if ((*it) == "Morale Boost")
		{
			moraleBoostCount++;
		}
		if ((*it) == "Double-Edge")
		{
			doubleEdgeCount++;
		}
	}

	point += moraleBoostCount;
	point -= doubleEdgeCount;
	return point;
}


