#include "JsonLoadManager.h"
#include "Card.h"

JsonLoadManager::JsonLoadManager()
{
}

JsonLoadManager::~JsonLoadManager()
{
}

// Files to Json object
nlohmann::json JsonLoadManager::LoadFileToJson(std::string _filePath)
{
	std::ifstream stream(_filePath);
	nlohmann::json jsonObject;
	stream >> jsonObject;

	return jsonObject;
}

// Convert Json object to set of cards
std::list<Card*>* JsonLoadManager::JsonToListCard(nlohmann::json _object)
{
	std::list<Card*>* setOfCards = new std::list<Card*>();
	if (_object.is_array() && (int)_object.size() > 0)
	{
		for (nlohmann::json object : _object)
		{	
			std::list<std::string> abilityList;
			if (object["ability"].is_array() && (int)object["ability"].size() > 0)
			{
				for (std::string ability : object["ability"])
				{
					abilityList.push_back(ability);
				}
			}
			Card* card = new Card(object["name"], object["row"], object["originalPoint"], abilityList,
								  object["image"], object["description"], object["rarity"]);

			setOfCards->push_back(card);
		}

		return setOfCards;
	}
	return nullptr;
}

std::list<Card*>* JsonLoadManager::JsonFileToListCard(std::string _filePath)
{
	std::list<Card*>* newListOfCard = new std::list<Card*>();
	nlohmann::json newJsonCards = LoadFileToJson(_filePath);
	newListOfCard = JsonToListCard(newJsonCards);
	
	return newListOfCard != nullptr ? newListOfCard : nullptr;
}
